# -*- mode: makefile-gmake; coding: utf-8 -*-

all:

.PHONY: clean
clean:
	find -name "*.blend?" -delete
